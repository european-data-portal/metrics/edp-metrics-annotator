# Metrics Annotator

Annotates incoming datasets with DQV metrics. 
The service is based on the [pipe-connector](https://gitlab.com/european-data-portal/pipe/edp-pipe-connector) library. Any configuration applicable for the pipe-connector can also be used for this service.

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [Configuration](#configuration)
    1. [Data Info Object](#data-info-object)
    1. [Environment](#environment)
    1. [Logging](#logging)

## Build

Requirements:
 * Git
 * Maven 3
 * Java 11

```bash
$ git clone ... 
$ cd edp-metrics-annotator
$ mvn package
```
 
## Run

```bash
$ java -jar target/annotator.jar
```

## Docker

Build docker image:
```bash
$ docker build -t edp/metrics-annotator .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 edp/metrics-annotator
```

## Configuration

### Environment

| Variable| Description | Default Value |
| :--- | :--- | :--- |
| `PIVEAU_LOAD_VOCABULARIES_FETCH` | Load all vocabularies from remote instead of using bundled versions. | `false` |
| `EXCLUDE_ANNOTATIONS` | Exclude annotations by providing a comma separated list. The five star quality annotation can be excluded by passing `fiveStar`. Example: `spatialAvailability,fiveStar` |  |

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable| Description | Default Value |
| :--- | :--- | :--- |
| `PIVEAU_PIPE_LOG_APPENDER` | Configures the log appender for the pipe context | `STDOUT` |
| `PIVEAU_LOGSTASH_HOST`            | The host of the logstash service | `logstash` |
| `PIVEAU_LOGSTASH_PORT`            | The port the logstash service is running | `5044` |
| `PIVEAU_PIPE_LOG_PATH`     | Path to the file for the file appender | `logs/piveau-pipe.%d{yyyy-MM-dd}.log` |
| `PIVEAU_PIPE_LOG_LEVEL`    | The log level for the pipe context | `INFO` |
| `PIVEAU_LOG_LEVEL`    | The general log level for the `io.piveau` package | `INFO` |

