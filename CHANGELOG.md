# ChangeLog

## Unreleased

## [1.1.0](https://gitlab.fokus.fraunhofer.de/viaduct/metrics/metrics-dqv-annotator/tags/1.1.0) (2020-04-23)

**Added:**
* Extended information about JSON-LD file-type

## [1.0.3](https://gitlab.fokus.fraunhofer.de/viaduct/metrics/metrics-dqv-annotator/tags/1.0.3) (2020-03-11)

**Changed:**
* Close dataset explicitly

## [1.0.2](https://gitlab.fokus.fraunhofer.de/viaduct/metrics/metrics-dqv-annotator/tags/1.0.2) (2020-03-06)

**Fixed:**
* Replace old measurements

## [1.0.1](https://gitlab.fokus.fraunhofer.de/viaduct/metrics/metrics-dqv-annotator/tags/1.0.1) (2020-03-04)

**Fixed:**
* Finally replace metrics graph to dataset

## [1.0.0](https://gitlab.fokus.fraunhofer.de/viaduct/metrics/metrics-dqv-annotator/tags/1.0.0) (2020-02-28)

Initial production release

**Added:**
* `attached` pipe config parameter
* Configuration `PIVEAU_LOAD_VOCABULARIES_FETCH`
* Configuration `EXCLUDE_ANNOTATIONS`
* Five Star `DQV.QualityAnnotation`
* Metric `atLeastFourStars`
* Annotate dcat ap compliance

**Changed:**
* Rearrange metadata quality
* Send rdf dataset with named graph
* Simplified pipe integration
* Use vocabularies library
* Annotations can now be excluded via configuration
* Measurements optionally replaceable
* Allow any name for metrics graph

**Removed:**
* Service proxy generation

**Fixed:**
* `dqv:computedOn`
* mediaType and format evaluation
* Test
