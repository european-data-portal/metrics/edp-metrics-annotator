package io.piveau.metrics.annotator.annotation;

import io.piveau.vocabularies.Concept;
import io.piveau.vocabularies.FileType;
import io.piveau.vocabularies.License;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.SKOS;

import java.util.Collections;
import java.util.stream.Stream;

public class FormatVisitor implements RDFVisitor {

    @Override
    public Concept visitBlank(Resource resource, AnonId anonId) {
        return resource.listProperties().toList().stream().flatMap(statement -> {
            Property predicate = statement.getPredicate();
            if (predicate == DC_11.description
                    || predicate == SKOS.prefLabel
                    || predicate == SKOS.altLabel
                    || predicate == SKOS.exactMatch) {
                return Stream.ofNullable(License.INSTANCE.getConcept(statement.getLiteral().getLexicalForm()));
            } else {
                return Stream.empty();
            }
        }).findFirst().orElse(null);
    }

    @Override
    public Concept visitURI(Resource resource, String uri) {
        return FileType.INSTANCE.getConcept(resource);
    }

    @Override
    public Concept visitLiteral(Literal literal) {
        return FileType.INSTANCE.getConcept(literal.getLexicalForm());
    }

}
