package io.piveau.metrics.annotator.annotation;

import io.piveau.dqv.PiveauMetrics;
import io.piveau.metrics.annotator.ApplicationConfig;
import io.piveau.pipe.PipeContext;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.utils.JenaUtils;
import io.piveau.vocabularies.*;
import io.piveau.vocabularies.vocabulary.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class AnnotatorVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public static final String ADDRESS = "io.piveau.pipe.annotating.dqv.queue";

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        startPromise.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        String data = pipeContext.getStringData();
        String mimeType = pipeContext.getMimeType();

        Dataset result = RDFMimeTypes.TRIG.equals(mimeType)
                ? JenaUtils.readDataset(data.getBytes(), null)
                : DatasetFactory.create();

        Model model = RDFMimeTypes.TRIG.equals(mimeType)
                ? result.getDefaultModel()
                : result.setDefaultModel(JenaUtils.read(data.getBytes(), mimeType)).getDefaultModel();

        ResIterator it = model.listSubjectsWithProperty(RDF.type, DCAT.Dataset);
        if (it.hasNext()) {
            Resource dataset = it.next();

            if (PiveauMetrics.listMetricsModels(result).isEmpty()) {
                String urn = "urn:"
                        + JenaUtils.normalize(pipeContext.getPipe().getHeader().getContext())
                        + ":" + pipeContext.getPipe().getHeader().getName();
                Model metrics =  PiveauMetrics.createMetricsGraph(result, urn);
                calculateMetrics(dataset, metrics);
            } else {
                Model metrics = PiveauMetrics.listMetricsModels(result).get(0);
                calculateMetrics(dataset, metrics);
                metrics.listStatements(dataset, DQV.hasQualityAnnotation, (RDFNode) null)
                        .filterKeep(statement -> statement.getResource().hasProperty(OA.hasBody))
                        .mapWith(statement -> statement.getResource().getPropertyResourceValue(OA.hasBody))
                        .filterKeep(res -> res.hasProperty(RDF.type, SHACL.ValidationReport))
                        .nextOptional().ifPresent(res -> {
                    if (res.hasProperty(SHACL.conforms)) {
                        addMeasurement(metrics, PV.dcatApCompliance, dataset, res.getProperty(SHACL.conforms).getBoolean());
                    }
                });
            }

            // Only TRIG and TRIX are suitable for serializing named graphs
            pipeContext
                    .setResult(
                            JenaUtils.write(result, Lang.TRIG),
                            RDFMimeTypes.TRIG,
                            pipeContext.getDataInfo().put("content", "metrics"))
                    .forward();
            pipeContext.log().info("Data annotated: {}", pipeContext.getDataInfo());
        } else {
            pipeContext.setFailure("No dataset found in payload");
        }
        result.close();
    }

    private void calculateMetrics(Resource dataset, Model dqvReport) {
        log.debug("Annotating dataset [{}]", dataset.getURI());

        List<String> excludedAnnotations = Arrays.asList(config().getString(ApplicationConfig.ENV_EXCLUDE_ANNOTATIONS, "").split(","));

        if (!excludedAnnotations.contains(PV.accessRightsAvailability.getLocalName())) {
            addMeasurement(dqvReport, PV.accessRightsAvailability, dataset, dataset.hasProperty(DCTerms.accessRights));
            if (!excludedAnnotations.contains(PV.accessRightsVocabularyAlignment.getLocalName())) {
                Resource accessRights = dataset.getPropertyResourceValue(DCTerms.accessRights);
                addMeasurement(dqvReport, PV.accessRightsVocabularyAlignment, dataset, accessRights != null && AccessRight.INSTANCE.isConcept(accessRights));
            }
        }

        if (!excludedAnnotations.contains(PV.categoryAvailability.getLocalName()))
            addMeasurement(dqvReport, PV.categoryAvailability, dataset, dataset.hasProperty(DCAT.theme));

        if (!excludedAnnotations.contains(PV.contactPointAvailability.getLocalName()))
            addMeasurement(dqvReport, PV.contactPointAvailability, dataset, dataset.hasProperty(DCAT.contactPoint));

        if (!excludedAnnotations.contains(PV.keywordAvailability.getLocalName()))
            addMeasurement(dqvReport, PV.keywordAvailability, dataset, dataset.hasProperty(DCAT.keyword) || dataset.hasProperty(DCTerms.subject));

        if (!excludedAnnotations.contains(PV.publisherAvailability.getLocalName()))
            addMeasurement(dqvReport, PV.publisherAvailability, dataset, dataset.hasProperty(DCTerms.publisher));

        if (!excludedAnnotations.contains(PV.spatialAvailability.getLocalName()))
            addMeasurement(dqvReport, PV.spatialAvailability, dataset, dataset.hasProperty(DCTerms.spatial));

        if (!excludedAnnotations.contains(PV.temporalAvailability.getLocalName()))
            addMeasurement(dqvReport, PV.temporalAvailability, dataset, dataset.hasProperty(DCTerms.temporal));

        if (!excludedAnnotations.contains(PV.dateIssuedAvailability.getLocalName()))
            addMeasurement(dqvReport, PV.dateIssuedAvailability, dataset, dataset.hasProperty(DCTerms.issued));

        for (Statement distributionStmt : dataset.listProperties(DCAT.distribution).toSet()) {
            Resource distribution = distributionStmt.getObject().asResource();

            // values required for five star rating
            // TODO keep these as blank nodes?
            Resource fiveStarOpenLicenceDerivedFrom = null;
            Resource fiveStarMachineInterpretableDerivedFrom = null;
            Resource fiveStarNonProprietaryDerivedFrom = null;

            boolean hasOpenLicence = false;
            boolean isFormatMachineInterpretable = false;
            boolean isFormatNonProprietary = false;
            boolean isRdfFormat = false; // set true when dct:format suggests RDF
            boolean hasLinkedResources = false; // there is currently no way for us to check this


            if (!excludedAnnotations.contains(PV.byteSizeAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.byteSizeAvailability, distribution, distribution.hasProperty(DCAT.byteSize));

            if (!excludedAnnotations.contains(PV.dateIssuedAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.dateIssuedAvailability, distribution, distribution.hasProperty(DCTerms.issued));

            if (!excludedAnnotations.contains(PV.dateModifiedAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.dateModifiedAvailability, distribution, distribution.hasProperty(DCTerms.modified));

            if (!excludedAnnotations.contains(PV.downloadUrlAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.downloadUrlAvailability, distribution, distribution.hasProperty(DCAT.downloadURL));

            if (!excludedAnnotations.contains(PV.rightsAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.rightsAvailability, distribution, distribution.hasProperty(DCTerms.rights));

            if (!excludedAnnotations.contains(PV.formatAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.formatAvailability, distribution, distribution.hasProperty(DCTerms.format));

            if (!excludedAnnotations.contains(PV.mediaTypeAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.mediaTypeAvailability, distribution, distribution.hasProperty(DCAT.mediaType));


            RDFNode format;
            boolean isFormatAligned = false;

            if (distribution.hasProperty(DCTerms.format)) {
                format = distribution.getProperty(DCTerms.format).getObject();

                Concept formatConcept = (Concept) format.visitWith(new FormatVisitor());
                if (formatConcept != null) {
                    isFormatAligned = true;
                    isFormatMachineInterpretable = FileType.isMachineReadable(formatConcept);
                    isFormatNonProprietary = FileType.isNonProprietary(formatConcept);
                    isRdfFormat = isRdfFormat(format.toString());

                    if (!excludedAnnotations.contains(PV.formatMediaTypeMachineInterpretable.getLocalName()))
                        fiveStarMachineInterpretableDerivedFrom = addMeasurement(dqvReport, PV.formatMediaTypeMachineInterpretable, distribution, isFormatMachineInterpretable);

                    if (!excludedAnnotations.contains(PV.formatMediaTypeNonProprietary.getLocalName()))
                        fiveStarNonProprietaryDerivedFrom = addMeasurement(dqvReport, PV.formatMediaTypeNonProprietary, distribution, isFormatNonProprietary);
                } else {
                    isFormatAligned = false;
                }
            }


            RDFNode mediaType;
            boolean isMediaTypeAligned = false;

            if (distribution.hasProperty(DCAT.mediaType)) {
                mediaType = distribution.getProperty(DCAT.mediaType).getObject();

                if (mediaType.isLiteral()) {
                    isMediaTypeAligned = IanaTypeFactory.INSTANCE.getIanaRegistry(mediaType.asLiteral().getLexicalForm()).isMediaType();
                } else if (mediaType.isURIResource()) {
                    isMediaTypeAligned = IanaTypeFactory.INSTANCE.getIanaRegistry(mediaType.asResource()).isMediaType();
                }
            }

            if (!excludedAnnotations.contains(PV.formatMediaTypeVocabularyAlignment.getLocalName()))
                addMeasurement(dqvReport, PV.formatMediaTypeVocabularyAlignment, distribution, isFormatAligned && isMediaTypeAligned);

            if (!excludedAnnotations.contains(PV.licenceAvailability.getLocalName()))
                addMeasurement(dqvReport, PV.licenceAvailability, distribution, distribution.hasProperty(DCTerms.license));

            if (distribution.hasProperty(DCTerms.license)) {
                if (!excludedAnnotations.contains(PV.knownLicence.getLocalName())) {
                    addMeasurement(dqvReport, PV.knownLicence, distribution, distribution.getProperty(DCTerms.license).getObject().visitWith(new LicenseKnownVisitor()));
                }

                if (!excludedAnnotations.contains(OSI.isOpen.getLocalName())) {
                    hasOpenLicence = (Boolean) distribution.getProperty(DCTerms.license).getObject().visitWith(new LicenseOpenVisitor());
                    fiveStarOpenLicenceDerivedFrom = addMeasurement(dqvReport, OSI.isOpen, distribution, hasOpenLicence);
                }
            }

            if (!excludedAnnotations.contains("fiveStar")) {
                Resource fiveStarAnnotation = dqvReport.createResource(DQV.QualityAnnotation);
                Resource fiveStarRating;

                // 0-Star is derived from the open licence measurement
                if (fiveStarOpenLicenceDerivedFrom != null)
                    fiveStarAnnotation.addProperty(PROV.wasDerivedFrom, fiveStarOpenLicenceDerivedFrom);

                if (hasOpenLicence) {

                    // 1-Star is derived from the machine-interpretability measurement
                    if (fiveStarMachineInterpretableDerivedFrom != null)
                        fiveStarAnnotation.addProperty(PROV.wasDerivedFrom, fiveStarMachineInterpretableDerivedFrom);

                    if (isFormatMachineInterpretable) {

                        // 2-Star is derived from the non-proprietary measurement
                        if (fiveStarNonProprietaryDerivedFrom != null)
                            fiveStarAnnotation.addProperty(PROV.wasDerivedFrom, fiveStarNonProprietaryDerivedFrom);

                        if (isFormatNonProprietary) {

                            // not derived from a single measurement

                            if (isRdfFormat) {

                                if (hasLinkedResources) {
                                    // is currently not evaluated

                                    fiveStarRating = PV.fiveStars;
                                } else {
                                    fiveStarRating = PV.fourStars;
                                }
                            } else {
                                fiveStarRating = PV.threeStars;
                            }
                        } else {
                            fiveStarRating = PV.twoStars;
                        }
                    } else {
                        fiveStarRating = PV.oneStar;
                    }
                } else {
                    fiveStarRating = PV.zeroStars;
                }

                if (!excludedAnnotations.contains(PV.atLeastFourStars.getLocalName()))
                    addMeasurement(dqvReport, PV.atLeastFourStars, distribution, fiveStarRating == PV.fourStars || fiveStarRating == PV.fiveStars);

                fiveStarAnnotation.addProperty(RDF.type, DQV.QualityAnnotation);
                fiveStarAnnotation.addProperty(OA.hasBody, fiveStarRating);
                fiveStarAnnotation.addProperty(OA.motivatedBy, OA.classifying);
                dqvReport.add(dataset, DQV.hasQualityAnnotation, fiveStarAnnotation);
            }
        }
    }

    private Resource addMeasurement(Model report, Resource metric, Resource computedOn, Object value) {
        return PiveauMetrics.replaceMeasurement(report, computedOn, metric, value);
    }

    private boolean isRdfFormat(String format) {
        return format != null && (
                format.toLowerCase().contains("rdf") ||
                        format.toLowerCase().contains("turtle") ||
                        format.toLowerCase().contains("ttl") ||
                        format.toLowerCase().contains("ntriples") ||
                        format.toLowerCase().contains("n3") ||
                        format.toLowerCase().contains("json-ld") ||
                        format.toLowerCase().contains("jsonld")
        );
    }
}
