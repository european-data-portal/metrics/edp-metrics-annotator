package io.piveau.metrics.annotator.annotation;

import io.piveau.vocabularies.Concept;
import io.piveau.vocabularies.License;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.SKOS;

public class LicenseOpenVisitor implements RDFVisitor {

    @Override
    public Boolean visitBlank(Resource resource, AnonId anonId) {
        return resource.listProperties().toList().stream().anyMatch(statement -> {
            Property predicate = statement.getPredicate();
            if (predicate == DC_11.description
                    || predicate == SKOS.prefLabel
                    || predicate == SKOS.altLabel
                    || predicate == SKOS.exactMatch) {

                Concept licence = License.INSTANCE.getConcept(statement.getLiteral().getLexicalForm()) != null
                        ? License.INSTANCE.getConcept(statement.getLiteral().getLexicalForm())
                        : License.INSTANCE.altLabel(statement.getLiteral().getLexicalForm(), "en");

                return licence != null && License.INSTANCE.isOpen(licence);
            } else {
                return false;
            }
        });
    }

    @Override
    public Boolean visitURI(Resource resource, String uri) {
        Concept licence = License.INSTANCE.isConcept(resource)
                ? License.INSTANCE.getConcept(resource)
                : License.INSTANCE.exactMatch(uri);

        return licence != null && License.INSTANCE.isOpen(licence);
    }

    @Override
    public Boolean visitLiteral(Literal literal) {

        Concept licence = License.INSTANCE.getConcept(literal.getLexicalForm());

        if (licence == null)
            licence = License.INSTANCE.exactMatch(literal.getLexicalForm());

        if (licence == null )
            License.INSTANCE.altLabel(literal.getLexicalForm(), "en");

        return licence != null && License.INSTANCE.isOpen(licence);
    }
}
