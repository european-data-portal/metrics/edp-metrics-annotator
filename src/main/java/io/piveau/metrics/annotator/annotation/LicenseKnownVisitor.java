package io.piveau.metrics.annotator.annotation;

import io.piveau.vocabularies.License;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.SKOS;

public class LicenseKnownVisitor implements RDFVisitor {

    @Override
    public Boolean visitBlank(Resource resource, AnonId anonId) {
        return resource.listProperties().toList().stream().anyMatch(statement -> {
            Property predicate = statement.getPredicate();
            if (predicate == DC_11.description
                    || predicate == SKOS.prefLabel
                    || predicate == SKOS.altLabel
                    || predicate == SKOS.exactMatch) {
                return License.INSTANCE.getConcept(statement.getLiteral().getLexicalForm()) != null
                        || License.INSTANCE.altLabel(statement.getLiteral().getLexicalForm(), "en") != null;
            } else {
                return false;
            }
        });
    }

    @Override
    public Boolean visitURI(Resource resource, String uri) {
        return License.INSTANCE.isConcept(resource) || License.INSTANCE.exactMatch(uri) != null;
    }

    @Override
    public Boolean visitLiteral(Literal literal) {
        return License.INSTANCE.exactMatch(literal.getLexicalForm()) != null
                || License.INSTANCE.getConcept(literal.getLexicalForm()) != null
                || License.INSTANCE.altLabel(literal.getLexicalForm(), "en") != null;
    }
}
