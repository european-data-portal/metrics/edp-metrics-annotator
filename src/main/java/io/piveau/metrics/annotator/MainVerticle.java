package io.piveau.metrics.annotator;

import io.piveau.metrics.annotator.annotation.AnnotatorVerticle;
import io.piveau.pipe.connector.PipeConnector;
import io.piveau.vocabularies.ConceptSchemes;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Launcher;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {

        /*
         * FIXME
         * Specifying the storeType to "env" prevents passing configurations from tests via DeploymentOptions
         */
//        JsonObject keys = new JsonObject().put("keys", new JsonArray()
//                .add(ApplicationConfig.ENV_PIVEAU_LOAD_VOCABULARIES_FETCH)
//                .add(ApplicationConfig.ENV_EXCLUDE_ANNOTATIONS));
//
//        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
//                .setType("env")
//                .setConfig(keys);
//        ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions)).getConfig(retrieveConfig -> {

        ConfigRetriever.create(vertx).getConfig(retrieveConfig -> {
            if (retrieveConfig.succeeded()) {

                ConceptSchemes.initRemotes(vertx, false, false);

                DeploymentOptions options = new DeploymentOptions()
                        .setConfig(retrieveConfig.result())
                        .setWorker(true);

                vertx.deployVerticle(AnnotatorVerticle.class, options, deploy -> {
                    if (deploy.succeeded()) {
                        PipeConnector.create(vertx, cr -> {
                            if (cr.succeeded()) {
                                cr.result().consumer(AnnotatorVerticle.ADDRESS);
                                startPromise.complete();
                            } else {
                                startPromise.fail(cr.cause());
                            }
                        });
                    } else {
                        startPromise.fail(deploy.cause());
                    }
                });
            } else {
                startPromise.fail(retrieveConfig.cause());
            }
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }
}
