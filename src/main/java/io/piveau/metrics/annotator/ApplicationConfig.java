package io.piveau.metrics.annotator;

public final class ApplicationConfig {

    public static final String ENV_PIVEAU_LOAD_VOCABULARIES_FETCH = "PIVEAU_LOAD_VOCABULARIES_FETCH";
    public static final Boolean DEFAULT_PIVEAU_LOAD_VOCABULARIES_FETCH = false;

    public static final String ENV_EXCLUDE_ANNOTATIONS = "EXCLUDE_ANNOTATIONS";
}
